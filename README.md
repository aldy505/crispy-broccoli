# Hello!

Right, I'll explain the project first.

## Prompt

This is the prompt:

"Silahkan membuat website dengan tema movie gallery, dari kami telah menyediakan mock API
tapi dibebaskan untuk untuk menggunakan API yang lain, dengan syarat memenuhi beberapa poin
yang telah kami siapkan."

Take home test harus mencangkup:
- List w/ thumbnail image
- Search
- Show detail
- Filter Tanggal

Kita menyediakan api, tapi boleh menggunakan/membuat api sendiri agar kreatifitas tidak terbatasi.
- Get list: https://5f50ca542b5a260016e8bfb0.mockapi.io/api/v1/movies
- Get detail: https://5f50ca542b5a260016e8bfb0.mockapi.io/api/v1/movies/1

Score lebih jika:
- Menggunakan autocomplete
- Menggunakan lazy load

## Why I use Vue, and all that stuff?

Well, Vue is my first framework when I first learn about Javascript (again, since my hiatus from learning it) in 2019.
On this current version that I use (Vue 3), the developer experience for developing Vue apps has gotten better.
It's so much better when combined with Typescript, where of course, we got static types annotation and all its' benefits.

For the styling, I use a CSS library called Windi CSS. It's kind of a fork of [Tailwind CSS](http://tailwindcss.com/)
where unlike Bootstrap, or Ant Design, it's not really a ready-to-go component library, it's just CSS utilities as a class.

So, in Tailwind (because it's also the same in Windi CSS), a mixed class of `w-full p-4 font-bold` results in:

```css
.w-full {
  width: 100%;
}
.p-4 {
  padding: 1rem 1rem;
}
.font-bold {
  font-weight: 700;
}
```

Some argue that by using Tailwind/Windi, it will make the HTML part dirty. I kind of agree with that, but for quick prototyping, I prefer my HTML to be dirty first, then I'll refactor it after I got everything working together.

About SASS, If you see through the source code, I don't really use it. I just install it so that my annotations on VSCode won't turned out being errors when I put the `@apply` directives. But, for CSS pre-processors, Sass and Stylus are the ones I love.

Then, rather than using Webpack like how people usually do for bundling their web app, I use [Vite](https://vitejs.dev/) instead. Compared to Webpack, Vite is faster, because during development, it uses ESBuild - another bundling library like Webpack but it's written on Golang, so it's definitely faster. If you don't believe me, go ahead and clone the repository, do `npm install` and `npm run dev`.

I know that this is my preference, but I'm always open to use any kind of framework and/or libraries.

## Directory structure

```
.
├── index.html          - Index entry point
├── package-lock.json
├── package.json
├── public              - Public directory
│  └── favicon.ico
├── README.md           - You are here!
├── src
│  ├── App.vue          - I treat this file as a default layout entry point
│  ├── components/      - Contains components
│  ├── env.d.ts
│  ├── main.ts          - App entry point
│  ├── pages/           - Contains the actual pages
│  ├── store/           - Contains Vuex (the state management library for Vue) files
│  ├── types/           - Old plain typescript typings
│  ├── utils/           - Utilities functions
│  └── vuex.d.ts
├── tsconfig.json
├── vite.config.ts      - Config for Vite
└── windi.config.ts     - Config for Windi CSS
```

## Deployment

The project is deployed through Cloudflare Pages. Why not Netlify, Heroku, Vercel, or others? Nah, because I love Cloudflare.

Hahaha, I'm joking. Cloudflare Pages offer free SSL certificate that's not a Let's Encrypt SSL. I have no grudges againts Let's Encrypt, but when I could get a better SSL certificate for free, even better - from Cloudflare themself, why not?

## End notes

Thank you for reading this README. I put some comments on the source code to guide you about my thoughts. I hope you won't get confused. If you have anything to ask, kindly throw your questions via WhatsApp through the number I provided.