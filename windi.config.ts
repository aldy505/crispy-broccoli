import {defineConfig} from 'windicss/helpers';

export default defineConfig({
	darkMode: 'media',
	theme: {
		extend: {
			fontFamily: {
				hind: ['Hind Siliguri'],
			},
			flex: {
				2: '2 2 0%',
				3: '3 3 0%',
				4: '4 4 0%',
				5: '5 5 0%',
			},
		},
	},
});
