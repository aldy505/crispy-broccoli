import ky from 'ky';
import {createStore} from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import type {Movie} from '../types/movies';

export interface MovieState {
  movies: Movie[];
	proxy: Movie[];
}

const BASE_URL = 'https://5f50ca542b5a260016e8bfb0.mockapi.io/api/v1/movies';

// This is the one of the first files that I wrote, while writing this whole frontend app.
export default createStore<MovieState>({
	state() {
		return {
			// Movies contains the actual movies data. This acts as a single source of truth.
			// So, nothing may change the content of this other than the Vuex action below.
			movies: [],
			// Proxy contains copied data of movies, this one can be destroyed, rearranged,
			// or whatever the code likes.
			proxy: [],
		};
	},
	getters: {
		getProxy: state => state.proxy,
		getMovieById: state => (id: number) => state.movies.find(o => o.id === String(id)),
		// When I wrote this, I think it's a good point to make the second parameter optional.
		// So if at one time someone asked to get a movie by exact date, I can still do that.
		getMovieByDate: state => (d1: Date, d2?: Date) => {
			if (!d2) {
				return state.movies.filter(o => o.showTime === d1.toISOString());
			}

			return state.movies.filter(o => o.showTime >= d1.toISOString() || o.showTime <= d2.toISOString());
		},
		searchByTitle: state => (title: string) => {
			if (!title) {
				return [];
			}

			return state.movies.filter(o => o.title.toLowerCase().includes(title.toLowerCase()));
		},
		checkMovies: state => state.movies.length > 0,
	},
	mutations: {
		setMovies(state, payload: Movie[]) {
			state.movies = payload;
			state.proxy = payload;
		},
		setProxy(state, payload: Movie[]) {
			if (payload.length === 0) {
				state.proxy = state.movies;
				return;
			}

			state.proxy = payload;
		},
	},
	actions: {
		async fetchMovies({commit}) {
			// Why did I use ky library (by the great sindresorhus) instead of a regular Fetch API?
			// Well, I like the elegancy of ky. And it automatically retries any unsuccesful HTTP request.
			// But it will eventually fails if the retry limit has been maxed out.
			const response = await ky.get(BASE_URL, {headers: {accept: 'application/json'}}).json() as Movie[];
			commit('setMovies', response);
			return response;
		},
		async fetchMovieById({state, commit, getters}, id: number) {
			if (state.movies.length > 0) {
				return getters.getMovieById(id);
			}

			const response = await ky.get(BASE_URL, {headers: {accept: 'application/json'}}).json() as Movie[];
			commit('setMovies', response);
			return getters.getMovieById(id);
		},
	},
	plugins: [createPersistedState({fetchBeforeUse: true})],
});

