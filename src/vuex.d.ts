import {Store} from 'vuex';
import {MovieState} from './store/movies';

declare module '@vue/runtime-core' {
  export interface State extends MovieState {}

  export interface ComponentCustomProperties {
    $store: Store<State>
  }
}
