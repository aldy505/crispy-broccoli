import {createApp} from 'vue';
import App from './App.vue';
import 'virtual:windi.css';
/**
 * These 3 lines below imports a CSS fonts called Hind Siliguri.
 * Why doing this rather than doing a link rel straight to Google Fonts?
 * There is an article by Fontsource themself about the advantages:
 * https://fontsource.org/docs/introduction
 *
 * TL;DR: Less render blocking because of network request.
 */
import '@fontsource/hind-siliguri/400.css';
import '@fontsource/hind-siliguri/600.css';
import '@fontsource/hind-siliguri/700.css';
import {createRouter, createWebHistory} from 'vue-router';
import store from './store/movies';

const router = createRouter({
	history: createWebHistory(),
	routes: [
		{path: '/', component: () => import('./pages/index.vue')},
		{path: '/movies', component: () => import('./pages/movies/index.vue')},
		{path: '/movies/:id', component: () => import('./pages/movies/[id].vue')},
	],
});

createApp(App).use(router).use(store).mount('#app');
