export interface Movie {
  id: string;
  showTime: string;
  title: string;
  image: string;
  like: number;
}
